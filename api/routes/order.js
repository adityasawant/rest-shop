const express = require('express');
const router = express.Router();

router.get('/', (req, resp, next) => {
    resp.status(200).json({
        message: 'Orders were fetch'
    });
});

router.post('/', (req, resp, next) => {
    resp.status(201).json({
        message: 'Order was created'
    });
});

router.get('/:orderId', (req, resp, next) => {
    resp.status(200).json({
        message: 'Order details',
        id: req.params.orderId
    });
});

router.delete('/:orderId', (req, resp, next) => {
    resp.status(200).json({
        message: 'Order deleted',
        id: req.params.orderId
    });
});

router.put('/:orderId', (req, resp, next) => {
    resp.status(200).json({
        message: 'Order put',
        id: req.params.orderId
    });
});

module.exports = router;