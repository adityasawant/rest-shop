const express = require('express');
const router = express.Router();

router.get('/', (req, resp, next) => {
    resp.status(200).json({
        message: 'Handling GET request to /products'
    });
});

router.post('/', (req, resp, next) => {
    resp.status(201).json({
        message: 'Handling POST request to /products'
    });
});

router.get('/:productId', (req, resp, next) => {
    // resp.status(200).json({
    //     message: 'Handling get request to /products: {id}'
    // });
    const id = req.params.productId;
    if (id === 'special') {
        resp.status(200).json({
            message: 'You discover special id.',
            id: id
        })
    } else {
        resp.status(200).json({
            message: 'You passed an id.',
            id: id
        })
    }
});

router.patch('/:productId', (req, resp, next) => {
    resp.status(200).json({
        message: 'Product updated.'
    });
});

router.delete('/:productId', (req, resp, next) => {
    resp.status(200).json({
        message: 'Product deleted.'
    });
});

module.exports = router;