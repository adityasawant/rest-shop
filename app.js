const express = require('express');
const morgan = require('morgan');

const app = express();

const productRoutes = require('./api/routes/product');
const orderRoutes = require('./api/routes/order');

app.use(morgan('dev'));


// app.use( (req, resp, next) => {
//     resp.status(200).json({
//         message: 'It works!'
//     });
// });

/* middleware */ 
app.use('/product', productRoutes); 
app.use('/order', orderRoutes);

/* Error handling - if route is not found */
app.use( (req, resp, next) => {
    const error = new Error('Not found');  // error obj default available in node.js
    error.status = 404;
    next(error);
});

/* other error */
app.use( (error, req, resp, next) => {
    resp.status(error.status || 500);
    resp.json({
        error : {
            message : error.message,
            statusCode : error.status
        }
    });
});

module.exports = app;
