const http = require('http'); // import http

const app = require('./app');

const port = process.env.port || 3000; // import from environment variable   // Need to check again

const server = http.createServer(app);

server.listen(port);